package SQLTesting;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MainEntry {
    public static void main (String args []){
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://192.168.0.12/Testing?" + "user=tempTest&password=test");
            Statement statement = connection.createStatement();
            statement.execute("INSERT INTO `Testing`.`test` (`name`, `surname`) VALUES ('Jamie', 'Stivala')");
        }catch (SQLException ex){
            System.err.println(ex.getSQLState());
        }
    }
}
