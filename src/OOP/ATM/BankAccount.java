package OOP.ATM;

class BankAccount {
    String Number;
    float balance;
    String Name;
    void deposit (float amount) {
        balance += amount;
    }
    void withdraw (float amount) {
        balance -= amount;
    }


    boolean canWithdraw(float amount){
        return (balance >= amount);
    }
}