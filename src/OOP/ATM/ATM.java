package OOP.ATM;

import Tools.Keyboard;

public class ATM {
    public static void main (String args [] ) {
        BankAccount b = new BankAccount();
        b.balance = (float)(Math.random() * 1000);
        
        System.out.println("Welcome To The OOP.ATM");
        
        System.out.println("Please Enter Your Name");
        b.Name = Keyboard.readString();

        
        System.out.println("Please Enter Your Card Number");
        b.Number = Keyboard.readString();
        
        System.out.println("Please Enter Your Balance");
        
        System.out.println("\n\nIf You Want To Withdraw Press 1\nIf You Want To Deposit Press 2\nIf You Want To Check Your Balance Press 3");
        int select = Keyboard.readInt();
        if (select == 1) {
            System.out.println("You Have Choosen Withdraw");
            System.out.println("How Much Do You Want To Withdraw");
            float amount = Keyboard.readInt();
            if(!b.canWithdraw(amount)){
                System.out.println("You Can't Withdraw");            
            }else{
                b.withdraw(amount);
            }  
        }else if (select == 2) {
            System.out.println("You Have Choosen Deposit");
            System.out.println("How Much Do You Want To Deposit");
            float amount = Keyboard.readInt();
            b.deposit(amount);
        }else if (select == 3) {
            System.out.println("You Have Choosen To Check Your Balance");
            System.out.printf("%.2f", b.balance);
        }
    }
}