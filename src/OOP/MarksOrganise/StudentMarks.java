package OOP.MarksOrganise;

/**
 * Write a description of class StudentMarks here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */


public class StudentMarks {

    //average mark(a)
    public static double getAverage(Object arrays[]) {
        double marks[] = (double[])(arrays[1]);
        double sum = 0;
        for (int i = 0; i != marks.length; i++) {
            sum = sum + marks[i];
        }
        return sum / marks.length;
    }

    //highest mark(b)
    public static String[] highestOrLowestMark(Object arrays[], boolean highest) {
        String names[] = (String[])(arrays[0]);
        double marks[] = (double[])(arrays[1]);
        double swapInt;
        String swapString;

        //Sorts marks in Order
        for (int outOfBounds = 0; outOfBounds < marks.length; outOfBounds++){
            for (int x = 0; x < (marks.length - outOfBounds - 1); x++){
                if(highest) {
                    if (marks[x] < marks[x + 1]) {
                        swapInt = marks[x];
                        marks[x] = marks[x + 1];
                        marks[x + 1] = swapInt;

                        swapString = names[x];
                        names[x] = names[x+1];
                        names[x + 1] = swapString;
                    }
                }else if (!highest){
                    if (marks[x] > marks[x + 1]) {
                        swapInt = marks[x];
                        marks[x] = marks[x + 1];
                        marks[x + 1] = swapInt;

                        swapString = names[x];
                        names[x] = names[x+1];
                        names[x + 1] = swapString;
                    }
                }
            }
        }
        //Sorts marks in Order
        String nameAndMarks[] = new String[2];
        nameAndMarks[0] = names[0];
        nameAndMarks[1] = marks[0] + "";
        return nameAndMarks; //Returns ascending or descending
    }

    //pass or fail(d)
    public static boolean[] passesFails(Object arrays[]) {
        double marks[] = (double[])(arrays[1]);
        boolean passed[] = new boolean[marks.length];
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] >= 50) {
                passed[i] = true; //If the student passed return passed
            } else if (marks[i] < 50) {
                passed[i] = false; //If student failed return failed
            }
        }
        return passed;
    }

    //marks above average(e)

    public static Object[] aboveOrBelow(Object arrays[], boolean aboveAverage) {
        String names[] = (String[])(arrays[0]);
        double marks[] = (double[])(arrays[1]);
        int arraySize = 0;

        for (int i = 0; i != names.length; i++) {
            if(aboveAverage) {
                if (marks[i] > getAverage(arrays)) {
                    arraySize++;
                }
            }else if (!aboveAverage){
                if (marks[i] <= getAverage(arrays)) {
                    arraySize++;
                }
            }
        }

        String returningNames[] = new String[arraySize];
        double returningMarks[] = new double[arraySize];

        int inArrayCounter = 0;

        for (int i = 0; i != marks.length; i++) {
            if(aboveAverage) {
                if (marks[i] > getAverage(arrays)) {
                    returningNames[inArrayCounter] = names[i];
                    returningMarks [inArrayCounter] = marks[i];
                    inArrayCounter++;
                }
            }else if (!aboveAverage){
                if (marks[i] <= getAverage(arrays)) {
                    returningNames[inArrayCounter] = names[i];
                    returningMarks [inArrayCounter] = marks[i];
                    inArrayCounter++;
                }
            }
        }
        return new Object[] {returningNames, returningMarks};
    }
}
