package OOP.MarksOrganise;
import Tools.Keyboard;

/**
 * Write a description of class ClassMarks here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ClassMarks
{
    public static void main(String args[]){
        System.out.println("Class Marks, Version 1.0.0, MarksOrganise Stivala 5G");
        System.out.println("==========================================");
        String choice = null;

        String names[] = new String[10];
        double marks[] = new double [10];

        for (int i = 0; i != names.length; i++) {
            System.out.print("Please enter name: ");
            names[i] = Keyboard.readString();
            System.out.print("Please enter mark: ");
            marks[i] = Keyboard.readDouble();
            System.out.println("\n");
        }

        Object arrays[] =  new Object[]{names, marks};

        System.out.println("Please choose what you'd like to view by inputting the corresponding letter.");
        System.out.println("a: Average mark");
        System.out.println("b: Highest mark");
        System.out.println("c: Lowest mark");
        System.out.println("d: Pass or Fail");
        System.out.println("e: Above average");
        System.out.println("f: Below average");
        System.out.println("m: Show menu");
        System.out.println("g: Exit (close program)\n\n\n");

        do{
                choice = Keyboard.readString();

                switch (choice) {
                    case "a":
                        System.out.println("The average mark is: " + StudentMarks.getAverage(arrays));
                        break;
                    case "b":
                        String highestMark[] = StudentMarks.highestOrLowestMark(arrays, true);
                        System.out.println("The student named " + highestMark[0] + " got the highest mark of " + highestMark[1]);
                        break;
                    case "c":
                        String lowestMark[] = StudentMarks.highestOrLowestMark(arrays, false);
                        System.out.println("The student named " + lowestMark[0] + " got the lowest mark of " + lowestMark[1]);
                        break;
                    case "d":
                        boolean passesOrFails[] = StudentMarks.passesFails(arrays);
                        for (int i = 0; i != passesOrFails.length; i++){
                            if (passesOrFails[i]){
                                System.out.println("The Student " + names[i] + " Passed");
                            }else if (!passesOrFails[i]){
                                System.out.println("The Student " + names[i] + " Failed");
                            }
                        }
                        break;
                    case "e":
                        String aboveName[] = (String[])StudentMarks.aboveOrBelow(arrays, true) [0];
                        double aboveMark[] = (double[])StudentMarks.aboveOrBelow(arrays, true) [1];
                        System.out.println("The average is " + StudentMarks.getAverage(arrays) + "\n\n");
                        for (int i = 0; i != aboveName.length; i++){
                            System.out.println("The student " + aboveName[i] + " got " + aboveMark[i]);
                        }
                        break;
                    case "f":
                        String belowName[] = (String[])StudentMarks.aboveOrBelow(arrays, false) [0];
                        double belowMark[] = (double[])StudentMarks.aboveOrBelow(arrays, false) [1];
                        System.out.println("The average is " + StudentMarks.getAverage(arrays) + "\n\n");
                        for (int i = 0; i != belowName.length; i++){
                            System.out.println("The student " + belowName[i] + " got " + belowMark[i]);
                        }
                        break;
                    case "g":
                        System.out.println("Exiting");
                        break;
                    case "m":
                        System.out.println("Please choose what you'd like to view by inputting the corresponding letter.");
                        System.out.println("a: Average mark");
                        System.out.println("b: Highest mark");
                        System.out.println("c: Lowest mark");
                        System.out.println("d: Pass or Fail");
                        System.out.println("e: Above average");
                        System.out.println("f: Below average");
                        System.out.println("m: Show menu");
                        System.out.println("g: Exit (close program)\n\n\n");
                        break;
                    case "h":
                        Tools.Histogram.histogram(marks);
                        break;
                    default:
                        System.out.println("Invalid choice. Ensure that you select the corresponding letter to obtain your choice answer");
                        break;
                }
                System.out.println("\n");
            }while(!choice.equalsIgnoreCase("g"));

    }
}
