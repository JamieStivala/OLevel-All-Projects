package HouseProducts;

import java.sql.*;

class SQLManager {
    private Connection SQLConnection = null;

    void createConnection(String host, String port, String database , String user, String password){
        try {
            SQLConnection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
        }catch (SQLException connectionError){
            connectionError.printStackTrace();
        }
    }
}
