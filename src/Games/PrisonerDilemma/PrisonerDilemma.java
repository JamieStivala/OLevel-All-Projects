package Games.PrisonerDilemma;

import Tools.Keyboard;

public class PrisonerDilemma {
    public static void main (String args[]){
        System.out.println("Enter 1 if prisoner A stayed silent\nEnter 2 if prisoner A betrayed");
        int prisonerA = Keyboard.readInt();

        System.out.println("Enter 1 if prisoner B stayed silent\nEnter 2 if prisoner B betrayed");
        int prisonerB = Keyboard.readInt();

         /*
         Cooperates/Silent --> 1
         Betray/Defects --> 2
          */

         if (prisonerA == 1 && prisonerB == 1){System.out.println("They Both Serve One Year");}
         else if (prisonerA == 1 && prisonerB == 2){System.out.println("Prisoner A Serves Three Year & Prisoner B Goes Free");}
         else if (prisonerA == 2 && prisonerB == 1){System.out.println("Prisoner A Servers Goes Free & Prisoner B Serves Three Years");}
         else if (prisonerA == 2 && prisonerB == 2){System.out.println("Both Serve Two Years");}
    }
}
