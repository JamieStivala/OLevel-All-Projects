package Games.Lottery;

import Tools.Keyboard;

public class Lottery {
    public static void main (String args []){

        System.out.println("What is the winning price");
        double winningPrice = Keyboard.readDouble();

        int userNumber[] = new int[5];
        int randoms[] = new int [5];
        boolean match[] = new boolean[5];

        System.out.println("Please enter the 5 numbers\n\n");

        //Enter the number and verify
        for (int i = 0; i != userNumber.length; i++) {
            boolean entered;
            do {
                entered = false;
                System.out.println("\nPlease enter number " + (i + 1) + " Out of " + userNumber.length);
                userNumber[i] = Keyboard.readInt();

                for (int x = 0; x != i; x++) {
                    if (userNumber[x] == userNumber[i]) {
                        entered = true;
                    }
                }

            }while (userNumber[i] > 45 || entered);
        }
        //Enter the numbers and verify

        for (int i = 0; i!= userNumber.length; i++) {
            boolean alreadyGenerated = false;
            int randomNumber = 0;
            //Generate random number and check if already created
            do {
                alreadyGenerated = false;
                randomNumber = 1 + (int) (Math.random() * (45 - 1));

                for (int x = 0; x != i; x++) {
                    if (randomNumber == randoms[x]) {
                        alreadyGenerated = true;
                    }else {
                        randoms [i] = randomNumber;
                    }
                }
            }while (alreadyGenerated);
            //Generate random number and check if already created

            //Check if it is a random number = user number
            if (randoms[i] == userNumber[i]) {
                match[i] = true;
            }
            //Check if random number = user number

        }

        //Count how many matched
        int count = 0;

        for (int i = 0; i != match.length; i++){
            if (match[i]){
                count++;
            }
        }
        //Count how many matches

        //Output The Price
        if (count == 0){
            System.out.println("Sir, you haven't won anything\n\n");

            System.out.println("The numbers generated where: ");
            for (int i = 0; i != randoms.length; i++){
                if (i != randoms.length - 1) {
                    System.out.print(randoms[i] + ", ");
                }else{
                    System.out.print(randoms[i]);
                }
            }

            System.out.println("\nAnd the numbers you guessed where: ");
            for (int i = 0; i != userNumber.length; i++){
                if (i != userNumber.length - 1) {
                    System.out.print(userNumber[i] + ", ");
                }else{
                    System.out.print(userNumber[i]);
                }
            }
        }else{

            System.out.println("The numbers generated where: ");
            for (int i = 0; i != randoms.length; i++){
                if (i != randoms.length - 1) {
                    System.out.print(randoms[i] + ", ");
                }else{
                    System.out.print(randoms[i]);
                }
            }

            System.out.println("\nAnd the numbers you guessed where: ");
            for (int i = 0; i != userNumber.length; i++){
                if (i == userNumber.length - 1) {
                    System.out.print(userNumber[i] + ", ");
                }else{
                    System.out.print(userNumber[i]);
                }
            }

            System.out.println("\nTherefore you got " + count + " correct");
            for (int i = 0; i != match.length; i++){
                if (i != match.length - 1) {
                    System.out.print(match[i] + ", ");
                }else {
                    System.out.print(match[i]);
                }
            }

            System.out.println("\n\n\nAnd because of this you won " + winningPrice/count);
        }
        //Output The Price
    }
}
