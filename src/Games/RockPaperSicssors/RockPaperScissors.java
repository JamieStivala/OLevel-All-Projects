package Games.RockPaperSicssors;

import Tools.ArrayTasks;
import Tools.Keyboard;
import Tools.NumberTasks;
import Tools.FileTasks;
/**
 * Rock Papers Scissors (Finished with integration of Files)
 * FINAL
 */
public class RockPaperScissors {
    public static void main (String args []){
        String fileName = "RockPaperScissors.txt";
        //Checks File Exists
        if (!FileTasks.checkFileAvailability(fileName)){
            FileTasks.createFile(fileName);
        }
        //Checks File Exists

        int score = 0;
        char restart = 'Z';

        System.out.println("Please enter your name");
        String name = Keyboard.readString();

        //Makes Sure It's A Valid Input
        String POI = null;
        do {
            System.out.println("Play (P) or Instructions (I)");
            POI = Keyboard.readString();
        }while (!POI.equalsIgnoreCase("I") && !POI.equalsIgnoreCase("Instructions") && !POI.equalsIgnoreCase("Play") && !POI.equalsIgnoreCase("P"));
        //Makes Sure It's A Valid Input

        //Shows Instructions
        if (POI.equalsIgnoreCase("Instructions") || POI.equalsIgnoreCase("I")) {
            System.out.println(
                    "Rock wins Scissors (Loses Rest)\n" +
                            "Paper wins Rock (Loses Rest)\n" +
                            "Scissors Wins Paper (Loses Rest)");
        }
        //Shows Instructions

        //RockPaperScissors Program
        do{

            //Makes Sure It's A Valid Input
            String chosen = null;
            do {
                System.out.println("\n\nPlease enter: Rock (R) ; Paper (P) ; Scissors (S) ");
                chosen = Keyboard.readString();
            }while (!chosen.equalsIgnoreCase("Rock") && !chosen.equalsIgnoreCase("R") && !chosen.equalsIgnoreCase("Paper") && !chosen.equalsIgnoreCase("P") && !chosen.equalsIgnoreCase("Scissors") && !chosen.equalsIgnoreCase("S"));
            //Makes Sure It's A Valid Input

            //Making them Easier To Work With
            int chosenInt = -1;
            if (chosen.equalsIgnoreCase("Rock") || chosen.equalsIgnoreCase("R")) {
                chosenInt = 0;
                chosen = "Rock";
            } else if (chosen.equalsIgnoreCase("Paper") || chosen.equalsIgnoreCase("P")) {
                chosenInt = 1;
                chosen = "Paper";
            } else if (chosen.equalsIgnoreCase("Scissors") || chosen.equalsIgnoreCase("S")) {
                chosenInt = 2;
                chosen = "Scissors";
            }
            //Making them Easier To Work with

            int randomNumber = NumberTasks.getRandomNumber(0, 3); //Generates Random Number from 0 - 2 (Using number tasks) --> Formula:  smallNumber + (int)(Math.random() * (bigNumber - smallNumber));

            /*
            0. Rock
            1. Paper
            2. Scissors

            Rock wins Scissors (Loses rest)
            Paper wins Rock (Loses Rest)
            Scissors Wins Paper (Loses Rest)
            0 wins 2
            1 wins 0
            2 wins 1
            */

            //Random Number To String
            String AIChosen = null;

            if (randomNumber == 0) {
                AIChosen = "Rock";
            } else if (randomNumber == 1) {
                AIChosen = "Paper";
            } else if (randomNumber == 2) {
                AIChosen = "Scissors";
            }
            //Random Number To String

            if (chosenInt == randomNumber) {
                System.out.println("\nIt's a draw");
            } else if (chosenInt == 0 && randomNumber == 2 || chosenInt == 1 && randomNumber == 0 || chosenInt == 2 && randomNumber == 1) {
                System.out.println("\nSince " + chosen + " Beats " + AIChosen + " You Have Won");
                score++;
            } else {
                System.out.println("\nSince " + AIChosen + " Beats " + chosen + " You Have Lost");
                score--;
            }

            System.out.println("If you would like to restart enter Y\nIf you would like to exit enter N");
            restart = Keyboard.readChar();
        }while (restart != 'N' && restart != 'n');
        //RockPaperScissors Program

        System.out.println(name + ", you have exited the game with " + score + " score.");

        //Checks If Name Already Exists
        boolean exists = false;
        String scoreFile[] = FileTasks.loadFileToString(fileName);
        int positionOfName = ArrayTasks.searchArray(scoreFile, name);
        int oldScore = -1;
        if(positionOfName != -1) {
            oldScore = Integer.parseInt(scoreFile[positionOfName + 1]);
            exists = true;
        }
        //Checks If Name Already Exists

        //If doesn't exist writes file
        if (!exists){
            FileTasks.addWorld(fileName, name);
            FileTasks.addWorld(fileName, "" + score);
        }
        //If doesn't exists write to file

        //Exceptions
        if(exists && score > oldScore){
            scoreFile[positionOfName + 1] = "" + score;
            System.out.println(scoreFile[positionOfName + 1]);
            FileTasks.loadStringToFile(fileName, scoreFile);
        }else if (exists && score < oldScore){
            System.out.println("\n\nA previous score of " + oldScore + " Has been found\nType Yes (Y) if you would still like to overwrite the new score");
            String overwrite = Keyboard.readString();

            if(overwrite.equalsIgnoreCase("Yes") || overwrite.equalsIgnoreCase("Y")){
                scoreFile[positionOfName + 1] = "" + score;
                FileTasks.loadStringToFile(fileName, scoreFile);
            }

        }
        //Exceptions
    }
}
