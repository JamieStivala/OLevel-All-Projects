package Games.Hangman;

/**
 * Custom tools made for Hangman
 */
public class CustomTools {

    public static Object[] bubbleSort (Object arrays[], boolean ascendingOrder){
        int swapInt;
        String swapString;

        String names[] = (String[])arrays[0];
        int scores[] = (int[])arrays[1];

        for (int outOfBounds = 0; outOfBounds < scores.length; outOfBounds++){
            for (int x = 0; x < (scores.length - outOfBounds - 1); x++){
                if(ascendingOrder) {
                    if (scores[x] > scores[x + 1]) {
                        swapInt = scores[x];
                        scores[x] = scores[x + 1];
                        scores[x + 1] = swapInt;

                        swapString = names[x];
                        names[x] = names[x+1];
                        names[x + 1] = swapString;
                    }
                }else if (!ascendingOrder){
                    if (scores[x] < scores[x + 1]) {
                        swapInt = scores[x];
                        scores[x] = scores[x + 1];
                        scores[x + 1] = swapInt;

                        swapString = names[x];
                        names[x] = names[x+1];
                        names[x + 1] = swapString;
                    }
                }
            }
        }
        //return arrayMerger(names, scores);
        return new Object[] {names, scores};
    }


    public static Object[] arraySplitter (String array[]){
        String names[] = new String[array.length/2];
        int scores[] = new int[array.length/2];

        boolean string = true;
        int arrayPosition = 0;

        for (int i = 0; i != array.length; i++){
            if(string){
                names [arrayPosition] = array[i];
                string = false;
            }else if(!string){
                scores [arrayPosition] = Integer.parseInt(array[i]);
                string = true;
                arrayPosition++;
            }
        }

        return new Object[]{names, scores};
    }


    public static String[] arrayMerger (String names[], int scores[]){
        String array[] = new String[names.length + scores.length];
        boolean namesD = false;
        boolean scoresD = false;
        boolean string = true;

        int stringCounter = 0;
        int integerCounter = 0;


        for(int i = 0; i != array.length; i++) {
            if(string){
                array[i] = names[stringCounter];
                stringCounter++;
                string = false;
            }else{
                array[i] = "" + scores[integerCounter];
                integerCounter++;
                string = true;
            }
        }
        return array;
    }


}
