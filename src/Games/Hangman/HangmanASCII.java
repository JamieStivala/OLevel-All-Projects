package Games.Hangman;

/**
 * Shows All The HangmanASCII
 */
class HangmanASCII {

    static String HangManCharacter(int amountOfTimesWrong) {
        String hangman[] = {
                //Begin Game
                "=========",
                //Begin Game

                //1 Wrong
                "      |\n" +
                        "      |\n" +
                        "      |\n" +
                        "      |\n" +
                        "      |\n" +
                        "=========",
                //1 Wrong

                //2 Wrong
                "  +---+\n" +
                        "  |   |\n" +
                        "      |\n" +
                        "      |\n" +
                        "      |\n" +
                        "      |\n" +
                        "=========",
                //2 Wrong

                //3 Wrong
                "  +---+\n" +
                        "  |   |\n" +
                        "  0   |\n" +
                        "  |   |\n" +
                        "      |\n" +
                        "      |\n" +
                        "=========",
                //3 Wrong

                //4 Wrong
                "  +---+\n" +
                        "  |   |\n" +
                        "  0   |\n" +
                        "  |   |\n" +
                        " /    |\n" +
                        "      |\n" +
                        "=========",
                //4 Wrong

                //5 Wrong
                "  +---+\n" +
                        "  |   |\n" +
                        "  0   |\n" +
                        "  |   |\n" +
                        " /|\\  |\n" +
                        "      |\n" +
                        "=========",
                //5 Wrong

                //6 Wrong
                "  +---+\n" +
                        "  |   |\n" +
                        "  0   |\n" +
                        "  |   |\n" +
                        " /|\\  |\n" +
                        " /    |\n" +
                        "=========",
                //6 Wrong

                //7 Wrong
                "  +---+\n" +
                        "  |   |\n" +
                        "  0   |\n" +
                        "  |   |\n" +
                        " /|\\  |\n" +
                        " / \\  |\n" +
                        "=========",
                //7 Wrong

                //Failed
                "You Have Failed",
                //Failed

                //Is Correct
                "The Word Entered Is Correct\nCongrats"
                //Is Correct
        };

        return hangman[amountOfTimesWrong];
    }
}
