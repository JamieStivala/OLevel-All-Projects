package Games.Hangman;

import java.util.Arrays;
import Tools.Keyboard;
import Tools.ArrayTasks;
import Tools.FileTasks;

/**
 * The Hangman || To Run - FIXING
 */
public class Main {
    public static void main(String args[]) throws Exception {
        String fileName = "Words.txt";


        //Check File Exist
        boolean fileAvailable = true;
        if (!FileTasks.checkFileAvailability(fileName)) {
            FileTasks.createFile(fileName);
            fileAvailable = false;
            System.err.println("File Not Found\nEnter 1 Word");
        }
        //Check File Exist

        char restartWholeProgram;
        do {
            ArrayTasks.outputArrayForAmountOfTime("\n", 20); //Clear Screen
            //--Restart Program Completely
            int score = 0;
            fileName = "Words.txt";
            //Decide Whether Play Game Or Add Word
            int chooseBeginning = 0;
            if (fileAvailable) {
                System.out.println("If You Would Like To Play The Game Press 1" +
                        "\nIf You Would Like To Add A World Press 2" +
                        "\nIf You Would Like To View The Scores Press 3");
                chooseBeginning = Keyboard.readInt();
            }
            //Decide Whether Play Game Or Add Word

            //Clear Screen
            ArrayTasks.outputArrayForAmountOfTime("\n", 10);
            //Clear Screen

            //Always Ask User Info
            System.out.println("Please Enter Your Full Name");
            String name = Keyboard.readString();
            //Always Ask User Info

            ArrayTasks.outputArrayForAmountOfTime("\n", 20); //Clear Screen

            //Play Game
            char restartGame;
            if (chooseBeginning == 1) {
                do {
                    String chosenWord = FileTasks.getRandomWord(fileName); //Get Random Word

                    //Start To Split It Into Array In Upper Case
                    char gameChars[] = new char[chosenWord.length()];
                    for (int i = 0; i != chosenWord.length(); i++) {
                        char x = chosenWord.charAt(i);
                        x = Character.toUpperCase(x);
                        gameChars[i] = x;
                    }
                    //Start To Split It Into Array In Uppercase

                    //All The Corrected Words
                    char correctChars[] = new char[chosenWord.length()];
                    //All The Corrected Words


                    //Makes All The Values Inside Array _
                    ArrayTasks.fillArray(correctChars, '_');
                    //Makes All The Values Inside Array _

                    //What The User Will Enter
                    char uiWrongChars[] = new char[52]; //English Alphabet x2
                    //What The User Will Enter


                    boolean isWordCorrect = false;
                    int amountOfTimesCharCorrect = 0;
                    int lastInuiCharArray = 0; //Counter For The Amount Of Times Wrong
                    int amountOfTimesWordIncorrect = 0;


                    do {
                        //------THE LOOP SHOULD START HERE (THE ONE WHERE IT ASKS ENTER CHAR OR WHOLE WORD -------
                        System.out.println("\n\n\nPress 1 To Enter A Character\nPress 2 To Enter The Whole Word");
                        int enterCharOrWord = Keyboard.readInt();

                        if (enterCharOrWord == 1) {
                            boolean wordInputted = false;

                            char UICharacter;
                            //Check If Already Inputted Loop Begin
                            boolean isAlreadyInputted = false;
                            do {
                                wordInputted = false;
                                //Enter Char
                                System.out.println("\nPlease Enter The Character");
                                UICharacter = Keyboard.readChar();
                                UICharacter = Character.toUpperCase(UICharacter); //Makes It Upper Case
                                System.out.println();  //Skip 2 Lines
                                //Enter Char

                                //System.out.println(correctChars[correctChars.length - 1]);

                                //Checks If It's Already In Wrong Inputted Array
                                for (int i = 0; i != lastInuiCharArray; i++) {
                                    if (lastInuiCharArray != 0 && uiWrongChars[i] == UICharacter) {
                                        System.out.println("Char Is Already Inputted\n"); //Skip 2 Lines
                                        wordInputted = true;
                                    }
                                }
                                //Checks If It's Already In Wrong Inputted Array

                                //Checks If Inputted Word Is Already In Correctly Inputted Word Array
                                for (int i = 0; i != correctChars.length; i++) {
                                    if (correctChars[i] == UICharacter) {
                                        System.out.println("Char Is Correct But Already Inputted\n"); //Skip 2 Lines
                                        wordInputted = true;
                                    }
                                }
                                //Checks If Inputted Word Is Already In Correctly Inputted Word Array

                            } while (wordInputted);
                            //Check If Already Inputted Loop End


                            //Check If Inputted Char Is Correct
                            boolean isCharCorrect = false;
                            if (!isAlreadyInputted) {
                                for (int i = 0; i != gameChars.length; i++) {
                                    if (gameChars[i] == UICharacter) {
                                        isCharCorrect = true;
                                    }
                                }
                                //Check If Inputted Char Is Correct
                            }

                            if (!isCharCorrect) {
                                System.out.println("The Inputted Word Is Incorrect");
                                //Puts It In Array
                                uiWrongChars[lastInuiCharArray] = UICharacter;
                                lastInuiCharArray++;  //Is Also A Counter Of The Amount Of Times Wrong
                                //Puts It In Array
                            }
                            //If Word Is Correct Add It To CorrectedChars Array
                            else if (isCharCorrect) {
                                System.out.println("Inputted Word Is Correct");
                                for (int i = 0; i != gameChars.length; i++) {
                                    if (gameChars[i] == UICharacter) {
                                        correctChars[i] = UICharacter;
                                        amountOfTimesCharCorrect++;
                                    }
                                }
                            }
                            //If Word Is Correct Add It To CorrectedChars Array

                            //Clean UI
                            ArrayTasks.outputArrayForAmountOfTime("\n", 2);
                            //Clear UI


                            //Output HangMan
                            System.out.println(HangmanASCII.HangManCharacter(lastInuiCharArray + amountOfTimesWordIncorrect));
                            //Output HangMan
                            System.out.println("\n");
                            //Output The Current Corrected Chars
                            ArrayTasks.outputArray(correctChars, true, true);
                            //Output The Current Corrected Chars

                            System.out.println("\n");

                            //Output The Wrong Chars
                            ArrayTasks.outputArray(uiWrongChars, true, true);
                            //Output The Wrong Chars

                        } else if (enterCharOrWord == 2) {
                            System.out.println("Please Enter The Word");
                            String uiWord = Keyboard.readString();

                            if (uiWord.equalsIgnoreCase(chosenWord)) {
                                isWordCorrect = true;
                            } else {
                                System.out.println("The Word You Entered Is Incorrect");
                                isWordCorrect = false;
                                amountOfTimesWordIncorrect++;
                            }
                        }
                    }
                    while ((lastInuiCharArray + amountOfTimesWordIncorrect) != 8 && !isWordCorrect && amountOfTimesCharCorrect != chosenWord.length()); //Game End

                    System.out.println("\n");
                    //Outputs The Winning Or The Loosing
                    if (isWordCorrect || lastInuiCharArray + amountOfTimesCharCorrect == 8 || amountOfTimesCharCorrect == chosenWord.length()) {
                        System.out.println("You Have Guessed The Word Successfully After Writing " + amountOfTimesCharCorrect + " Good Characters And Guessing The Word Wrong " + amountOfTimesWordIncorrect);
                        score++;
                    } else if (amountOfTimesCharCorrect == chosenWord.length()) {
                        System.out.println("You Have Guessed The Word Successfully After Writing " + amountOfTimesCharCorrect + " Good Characters And " + lastInuiCharArray + " Wrong Characters");
                        score++;
                    } else if ((lastInuiCharArray + amountOfTimesWordIncorrect) == 8) {
                        for (int i = 0; i != 10; i++) {
                            System.out.print(HangmanASCII.HangManCharacter(7));
                            Thread.sleep(1000);
                            ArrayTasks.outputArrayForAmountOfTime("\n", 10);
                            Thread.sleep(1000);
                        }
                    }
                    //Outputs The Winning Or The Loosing

                    System.out.println("Your Current Score Is " + score + "\n\n");

                    System.out.println("If You Would Like To Restart The Game Press Y\nIf You Would Like To Exit The Game Press N");
                    restartGame = Keyboard.readChar();
                } while (restartGame == 'y' || restartGame == 'Y');

            }

            //Play Game


            if (chooseBeginning == 1) {
                //Adds Score To Text File


                //Checks File Availability
                fileName = "Score.txt";
                if (!FileTasks.checkFileAvailability(fileName)) {
                    FileTasks.createFile(fileName);
                }
                //Checks File Availability


                //--The Full User Name Is A String Called Name
                //--The Score The User Got Is In Int Score

                String scoreArray[] = FileTasks.loadFileToString(fileName); //Load Array

                //Split Score Array
                Object splitted[] = CustomTools.arraySplitter(scoreArray);
                String names[] = (String[])splitted[0];
                int scores[] = (int[])splitted[1];
                //Split Score Array

                //Check If Name With Score Exists And Replace If Better
                boolean foundName = false;
                for (int i = 0; i != scores.length; i++) {
                    if (names[i].equalsIgnoreCase(name) && score > scores[i]) {
                        names[i] = name;
                        scores[i] = score;
                        foundName = true;
                    } else if (names[i].equalsIgnoreCase(name)) {
                        foundName = true;
                    }
                }
                //Check If Name With Score Exist And Replace If Better

                //If Name Is Not Found Create A New Line
                if (!foundName) {
                    String scoreS = score + "";
                    FileTasks.addWorld(fileName, name);
                    FileTasks.addWorld(fileName, scoreS);
                }
                //If Name Is Not Found Create A New Line

                if (foundName) {

                    //Combine Arrays
                    Arrays.fill(scoreArray, null);
                    scoreArray = CustomTools.arrayMerger(names, scores);
                    //Combine Arrays

                    FileTasks.loadStringToFile(fileName, scoreArray);
                }
                //Adds Score To Text File
            }


            //Add Word
            if (!fileAvailable || chooseBeginning == 2) {
                System.out.println("\n\nPlease Enter The New Word");
                String newWord = Keyboard.readString();
                FileTasks.addWorld(fileName, newWord);
            }
            //Add Word

            //Load Score
            if (chooseBeginning == 3) {
                fileName = "Score.txt";

                System.out.println("" +
                        "If You Would You Like To View The Top 10 Scores Press 1" +
                        "\nIf You Would Like To View All The Scores Press 2" +
                        "\nIf You Would Like To View A Specific Score Press 3");
                int choose = Keyboard.readInt();

                System.out.println("\n\n\n\n\n");

                if (choose == 1 || choose == 2) {
                    String scoreArray[] = FileTasks.loadFileToString(fileName);

                    //Splits Array
                    Object splitter[] = CustomTools.arraySplitter(scoreArray);
                    Object bubbleSort[] = CustomTools.bubbleSort(splitter, false);
                    //Splits Array

                    //Moves objects to String
                    String names[] = (String[])bubbleSort[0];
                    int scores[] = (int[])splitter[1];
                    //Move objects to String

                    if(choose == 1 && scores.length > 9){
                        for(int i = 0; i != 10; i++){
                            System.out.println("In Position " + (i + 1) + " We have " + names [i] + " With score " + scores[i]);
                        }
                    }else if (choose == 2 || scores.length <= 9){
                        for (int i = 0; i != names.length; i++){
                            System.out.println("In Position " + (i + 1) + " We have " + names [i] + " With score " + scores[i]);
                        }
                    }

                } else if (choose == 3) {
                    System.out.println("Please Enter The Full Name Of The Person");
                    String fullName = Keyboard.readString();

                    fileName = "Score.txt";

                    String scoreArray[] = FileTasks.loadFileToString(fileName);

                    for (int i = 0; i != scoreArray.length; i++) {
                        if (scoreArray[i].equalsIgnoreCase(fullName)) {
                            System.out.println("\n\n\n" + scoreArray[i] + " Has " + scoreArray[i + 1] + " Points");
                        }
                    }

                }
            }
            System.out.println("\n\n");
            System.out.println("If You Would Like To Restart The Program Press Y\nIf You Would Like To Exit Press N");
            restartWholeProgram = Keyboard.readChar();
        } while (restartWholeProgram == 'Y' || restartWholeProgram == 'y');


        //Load Score
    }
}