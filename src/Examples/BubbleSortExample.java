package Examples;

import Tools.Keyboard;

/**
 * Bubble Sort Example Class
 */

public class BubbleSortExample {
    public static void main (String args []){


        System.out.println("Enter the amount of numbers you want to be sorted: ");
        int amount = Keyboard.readInt();  //This step can be skipped if the array is already created

        System.out.println("Now Proceed To Entering The Numbers: ");
        double numbers[] = Tools.OperationsClass.enterDoubleNumbers(amount, "Enter Number: "); //This step can be skipped if the array already has numbers

        double sorted[] = Tools.BubbleSort.bubbleSort(numbers, false);


        //Outputting Sorted
        for (int i = 0; i != sorted.length; i++){
            System.out.println(sorted[i]);
        }
        //Outputting Sorted
    }
}
