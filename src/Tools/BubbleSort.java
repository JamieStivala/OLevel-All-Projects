package Tools;

/**
 * Sorting Integers Via Bubble Sort - OOP Implementation
 *
 * bubbleSort --> Bubble sorts numbers[]
 */
public class BubbleSort {

    //-------------------------------------------Bubble Sort-------------------------------------------
        public static double[] bubbleSort (double numbers[], boolean ascendingOrder){
            double swap;

            for (int outOfBounds = 0; outOfBounds < numbers.length; outOfBounds++){
                for (int x = 0; x < (numbers.length - outOfBounds - 1); x++){
                    if(ascendingOrder) {
                        if (numbers[x] > numbers[x + 1]) {
                            swap = numbers[x];
                            numbers[x] = numbers[x + 1];
                            numbers[x + 1] = swap;
                        }
                    }else if (!ascendingOrder){
                        if (numbers[x] < numbers[x + 1]) {
                            swap = numbers[x];
                            numbers[x] = numbers[x + 1];
                            numbers[x + 1] = swap;
                        }
                    }
                }
            }
            return numbers;
    }
    //-------------------------------------------Bubble Sort-------------------------------------------

}

