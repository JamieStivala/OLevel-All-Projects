package  Tools;

public class Histogram {

    public static void histogram (double[] array){
        for (int i = 1; i < 100; i += 10){
            int starCounter = 0;
            System.out.print(i + "-" + (i+9) + "\t|"); //Outputs 1-10, 11-20 etc
            for (int j = 0; j < array.length; j++){
                if (array [j] >= i && array[j] < i + 10) {
                    System.out.print("*");
                    starCounter++;
                }
            }
            if (starCounter != 0) {
                System.out.print("\t" + starCounter);
            }
            System.out.println();

        }
    }
}