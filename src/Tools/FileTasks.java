package Tools;

import java.io.*;

/**
 * This class will be used for anything that has to do with Files
 */


public class FileTasks {


    public static boolean checkFileAvailability(String fileName) {
        File f = new File(fileName);
        return f.exists(); //Simplified
    }

    public static void createFile(String fileName) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
        } catch (IOException e) {
            System.err.println("Can't Create File");
        }
    }

    //First Check If File Exists
    public static int getAmountOfLines(String fileName, boolean forArray) {
        int amountOfLines = 0;

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));


            while (bufferedReader.readLine() != null) {
                amountOfLines++;
            }
        } catch (IOException e) {
            System.err.println("Can't Get Amount Of Lines");
        }


        if (forArray) {
            return amountOfLines - 1; //So Location 0 = 0
        } else {
            return amountOfLines;
        }

    }
    //First Check If File Exists


    public static String readFile(String fileName, int LineNumber) {
        String x = null;

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            for (int i = 1; (x = bufferedReader.readLine()) != null; i++) {
                if (LineNumber == i) {
                    return x;
                }
            }

        } catch (IOException e) {
            System.err.println("Can't Read File");
        }
        return null;
    }

    public static void addWorld(String fileName, String newWord) {

        int amountOfLines = getAmountOfLines(fileName, false); //Is Updated Once Files Is Created
        int lineNumber = 1;
        String Words[] = new String[amountOfLines + 1]; //Were All The Words Are Stored

        while (lineNumber <= amountOfLines) {
            Words[lineNumber - 1] = readFile(fileName, lineNumber);
            lineNumber++;
        }

        Words[amountOfLines] = newWord; //Add New Word

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));

            for (int i = 0; i != Words.length; i++) {
                bufferedWriter.write(Words[i] + "\n");
            }
            bufferedWriter.close();

        } catch (IOException e) {
            System.err.println(e + "\n Can't Overwrite File");
        }
    }

    public static String getRandomWord(String fileName) {

        //Load Into Array
        int amountOfLines = getAmountOfLines(fileName, false); //Is Updated Once Files Is Created
        int lineNumber = 1;
        String Words[] = new String[amountOfLines + 1]; //Were All The Words Are Stored

        while (lineNumber <= amountOfLines) {
            Words[lineNumber - 1] = readFile(fileName, lineNumber);
            lineNumber++;
        }
        //Load Into Array

        int RandomNumber = (int) (Math.random() * amountOfLines);

        return Words[RandomNumber];
    }


    public static String[] loadFileToString(String fileName) {
        String loadedfile[] = new String[getAmountOfLines(fileName, false)];

        for (int i = 1; i != loadedfile.length + 1; i++) {
            loadedfile[i - 1] = readFile(fileName, i);
        }

        return loadedfile;
    }

    public static void loadStringToFile(String fileName, String arrayOfWords[]) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
            for (int i = 0; i != arrayOfWords.length; i++) {
                bufferedWriter.write(arrayOfWords[i]);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();

        } catch (IOException e) {
            System.err.println("Can't Write File");
        }
    }

    /**
     *
     * @param filename The name of the file
     * @param format The text needed from the file
     * @return Returns the object of the selected
     */

    public static String[] searchInTextFile(String filename, String format[]) {
        String file[] = loadFileToString(filename); //Importing the text file
        double loopAmount = (double) file.length / (double) format.length ;

        //Get amount of yes
        int amountOfYes = 0;
        for (int i = 0; i != format.length; i++){
            if (format [i].equalsIgnoreCase("yes")){
                amountOfYes++;
            }
        }
        //Get amount of yes and create array
        String outputs[] = new String [(int)(amountOfYes * loopAmount)];

        if (loopAmount != Math.ceil(loopAmount)){
            System.err.println("Format and file don't match");
            String exitArray[] = {"Format and file don't match"}; return exitArray;
        }else {
            int arrayLocation = 0;

            //Start The Search
            for (int i = 0, y = 0; i != loopAmount; i++) {
                for (int x = 0; x != format.length; x++){
                    if (format [x].equalsIgnoreCase("yes")){
                        outputs [arrayLocation] = file [y];
                        arrayLocation++;
                    }
                    y++;
                }
            }
            //Start the search

            /*
            Once search is done, array outputs will have

            X1 --> Name
            X1 --> Surname
            X2 --> Name
            X2 --> Surname

            Example of using

            for (int i = 0, x = 0; i != outputs.length; i += amountOfYes, x++){
                System.out.println(x + " " + outputs[i]);
                System.out.println(x + " " + outputs[i + 1]);
            }

            */
        }
        return outputs;
    }
}